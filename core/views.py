import googlemaps
from rest_framework import viewsets, status, mixins, permissions
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from django.conf import settings

import core.models as models
import core.serializers as serializers
from core.braintree_client import braintree


braintree.Configuration.configure(braintree.Environment.Sandbox,
                                  merchant_id=settings.BRAINTREE_MERCHANT_ID,
                                  public_key=settings.BRAINTREE_PUBLIC_KEY,
                                  private_key=settings.BRAINTREE_PRIVATE_KEY)


def _paid(self, request, amount):
    usr = request.user
    customer = braintree.Customer.find(usr.id)
    payment_method_token = customer.payment_methods[0].token
    result = braintree.Transaction.sale({
        "amount": amount,
        "payment_method_token": payment_method_token})
    return result.is_success


class ParkingViewSet(mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    queryset = models.Parking.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    serializer_class = serializers.ParkingSerializer

    def get_queryset(self):
        parking_spots = models.Parking.objects.all()
        address = self.request.query_params.get('address', None)
        if address:
            gmaps = googlemaps.Client(settings.GOOGLE_MAPS_API_KEY)
            result = gmaps.geocode(address)
            if not result:
                return Response('Address could not be found.',
                                status=status.HTTP_400_BAD_REQUEST)
            latitude = result[0]['geometry']['location']['lat']
            longitude = result[0]['geometry']['location']['lng']
            parking_spots = models.Parking.all_within_radius(latitude, longitude)
        available_spots = [spot.id for spot in parking_spots
                           if spot.is_available()]
        return parking_spots.filter(id__in=available_spots)


    @detail_route(methods=['post'],
                  permission_classes=[permissions.IsAuthenticated])
    def reserve(self, request, pk):
        parking = self.get_object()
        context={'request': request}
        serializer = serializers.ReserveParkingSerializer(
            data=request.data, context=context)
        if serializer.is_valid():
            amount_paid = serializer.validated_data['paid']
            # parking_paid_for = _paid(request, amount_paid)
            # if not paid_for_parking:
            #     return Response("Payment could not be completed.",
            #                     status=status.HTTP_400_BAD_REQUEST)
            reserved = parking.reserve(amount_paid, request.user)
            if not reserved:
                return Response("This parking spot is reserved already.",
                                status=status.HTTP_400_BAD_REQUEST)
            return Response({
                'paid': amount_paid,
                'status': 'reserved',
                'parking_id': parking.id,
                'reservation': (
                    serializers.ReservationSerializer(reserved,
                                                      context=context).data)})

        else:
            Response(serializer.errors,
                     status=status.HTTP_400_BAD_REQUEST)


class ReservationViewSet(mixins.RetrieveModelMixin,
                         viewsets.GenericViewSet):
    queryset = models.Reservation.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    serializer_class = serializers.ReservationSerializer

    # API should take extend_reservation parameter in minutes and extend the
    # reservation.
    @detail_route(methods=['post'],
                  permission_classes=[permissions.IsAuthenticated])
    def extend(self, request, pk):
        reservation = self.get_object()
        if not reservation.is_active():
            return Response('This reservation is not longer active and can '
                            'not be extended.',
                            status=status.HTTP_400_BAD_REQUEST)

        context={'request': request}
        serializer = serializers.ReservationExtendSerializer(
            data=request.data, context=context)
        if serializer.is_valid():
            minutes = serializer.validated_data['minutes']
            reservation.extend(minutes)
            return Response({'status': 'active'})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    @detail_route(permission_classes=[permissions.IsAuthenticated])
    def cancel(self, request, pk):
        reservation = self.get_object()
        reservation.cancel()
        return Response({'status': 'cancelled'})


class UserViewSet(mixins.CreateModelMixin,
                  viewsets.GenericViewSet):
    queryset = models.User.objects.all()
    serializer_class = serializers.UserSerializer
