from django.contrib import admin

# Register your models here.
import models

admin.site.register(models.Parking)
admin.site.register(models.Reservation)
admin.site.register(models.User)
