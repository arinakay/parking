from datetime import timedelta
from django.test import TestCase
from django.urls import reverse

from rest_framework import status
from rest_framework.test import APITestCase

from core.models import User, Parking, Reservation


class UserTestCase(APITestCase):
    def test_create_user(self):
        """
        Ensure we can create a new account object.
        """
        url = reverse('user-list')
        data = {'email': 'foobar@example.com',
                'password': 'password123',
                'phone': '5555555555',
                'credit_card': {
                    'number': '4111111111111111',
                    'expiration_date': '12/20'
                }}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().email, 'foobar@example.com')


class TestBaseMixin(object):
    def setUp(self):
        url = reverse('user-list')
        data = {'email': 'foobar@example.com',
                'password': 'password123',
                'phone': '5555555555'}
        response = self.client.post(url, data, format='json')
        self.user = User.objects.first()
        white_house = (38.897660, -77.036508)
        self.parking = Parking.objects.create(latitude=white_house[0],
                                              longitude=white_house[1],
                                              price_per_hour=1.00)


class ParkingTestCase(TestBaseMixin, APITestCase):

    def _reserve_parking(self):
        url = reverse('parking-reserve', kwargs={'pk': self.parking.id})
        data = {"paid": 10.00}
        self.client.login(username='foobar@example.com',
                          password='password123')
        return self.client.post(url, data, format='json')

    def test_reserve(self):
        response = self._reserve_parking()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        parking = Parking.objects.get(id=response.data['parking_id'])
        self.assertFalse(parking.is_available())

    def test_reserve_already_reserved(self):
        self._reserve_parking()
        url = reverse('parking-reserve', kwargs={'pk': self.parking.id})
        data = {"paid": 10.00}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_all_available(self):
        reserve_response = self._reserve_parking()
        parking = Parking.objects.get(id=reserve_response.data['parking_id'])
        list_url = reverse('parking-list')
        response = self.client.get(list_url)
        response_ids = [parking['id'] for parking in response.data]
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(parking.id not in response_ids)


class ReservationTestCase(TestBaseMixin, APITestCase):
    def create_reservation(self):
        url = reverse('parking-reserve', kwargs={'pk': self.parking.id})
        data = {"paid": 10.00}
        self.client.login(username='foobar@example.com',
                          password='password123')
        response = self.client.post(url, data, format='json')
        return Reservation.objects.get(id=response.data['reservation']['id'])

    def get_reservation(self, reservation_id):
        url = reverse('reservation-detail', kwargs={'pk': reservation_id})
        return self.client.get(url)

    def test_view_reservation(self):
        reservation = self.create_reservation()
        response = self.get_reservation(reservation.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], reservation.id)

    def cancel_reservation(self, reservation_id):
        url = reverse('reservation-cancel', kwargs={'pk': reservation_id})
        return self.client.get(url)

    def test_cancel_reservation(self):
        reservation = self.create_reservation()
        response = self.cancel_reservation(reservation.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['status'], 'cancelled')

    def extend_reservation(self, reservation, minutes=10):
        url = reverse('reservation-extend', kwargs={'pk': reservation.id})
        data = {"minutes": minutes}
        return self.client.post(url, data, format='json')

    def test_extend_reservation_okay(self):
        reservation = self.create_reservation()
        time_reserved = reservation.time_reserved
        expired_time = reservation.expired_time
        response = self.extend_reservation(reservation)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        reservation = Reservation.objects.get(id=reservation.id)
        self.assertEqual(time_reserved + 10, reservation.time_reserved)
        self.assertEqual(expired_time + timedelta(minutes=10),
                         reservation.expired_time)

    def test_extend_negative_minutes_error(self):
        reservation = self.create_reservation()
        response = self.extend_reservation(reservation, -10)
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data,
                         {"minutes":["Extention time must be more than 0 "
                                     "minutes."]})

    def test_extend_inactive_reservation_error(self):
        reservation = self.create_reservation()
        self.cancel_reservation(reservation.id)
        response = self.extend_reservation(reservation)
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data,
                         'This reservation is not longer active and can '
                         'not be extended.')

