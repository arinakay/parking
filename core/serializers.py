from django.conf import settings
from rest_framework import serializers

import models
from core.braintree_client import braintree


class ParkingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model =  models.Parking
        fields = ('longitude', 'latitude', 'price_per_hour', 'url', 'id')


class ReserveParkingSerializer(serializers.Serializer):
    paid = serializers.FloatField()

    def valdiate_paid(self, value):
        if value < 0:
            raise serialisers.ValidationError("Paid amount must be more than"
                                              "0.")
        return value


class ReservationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Reservation
        fields = ('url', 'reserved_at', 'expired_time', 'parking', 'id')


class ReservationExtendSerializer(serializers.Serializer):
    minutes = serializers.IntegerField()

    def validate_minutes(self, value):
        if value < 0:
            raise serializers.ValidationError("Extention time must be more "
                                              "than 0 minutes.")
        return value

class CreditCardSerializer(serializers.Serializer):
    number = serializers.CharField()
    expiration_date = serializers.CharField()


class UserSerializer(serializers.ModelSerializer):
    credit_card = CreditCardSerializer(write_only=True)

    class Meta:
        model = models.User
        fields = ('email', 'phone', 'password', 'credit_card')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        credit_card = validated_data.pop('credit_card')
        result = braintree.Customer.create({'credit_card': credit_card})
        if result.is_success:
            validated_data['braintree_customer_id'] = result.customer.id
        return models.User.objects.create_user(**validated_data)
