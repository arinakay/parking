from __future__ import unicode_literals
from datetime import datetime, timedelta
from decimal import Decimal

from django.utils import timezone
from django.db import models
from django.core.validators import MinValueValidator
from django.contrib.auth.models import (
        BaseUserManager, AbstractBaseUser
)
from geopy import Point
from geopy.distance import distance, VincentyDistance


# this is copypasta'd from django's site
# uses email as username
class EmailUserManager(BaseUserManager):
    def create_user(self, email, password=None, phone=None,
                    braintree_customer_id=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            braintree_customer_id=braintree_customer_id
        )
        user.set_password(password)
        user.phone = phone
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, phone=None):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser):
    email = models.EmailField(
                verbose_name='email address',
                max_length=255,
                unique=True,
            )
    phone = models.CharField(max_length=20)
    braintree_customer_id = models.CharField(max_length=200,
                                             null=True,
                                             default=None)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    objects = EmailUserManager()

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    def __str__(self):              # __unicode__ on Python 2
        return self.email

    @property
    def is_staff(self):
        return self.is_admin

class Parking(models.Model):
    longitude = models.FloatField()
    latitude = models.FloatField()
    price_per_hour = models.DecimalField(validators=[MinValueValidator(0)],
                                         decimal_places=2,
                                         max_digits=5)

    def is_available(self):
        """
        Check if this parking spot is available.
        Gets the most recent reservation for this parking spot if it exists
        and checks if the reservation is active.
        """
        reservations = Reservation.objects.filter(parking=self)
        if reservations.exists():
            reservation = reservations.latest('reserved_at')
            return not reservation.is_active()
        return True

    def current_reservation(self):
        """
        Get my current reservation, if I have one.
        """
        reservations = Reservation.objects.filter(parking=self)
        if reservations.exists():
            latest_reservation = reservations.latest('reserved_at')
            if latest_reservation.is_active():
                return latest_reservation

    def reserve(self, amount_paid, user):
        """
        Reserve this parking spot for a specified amount.
        :param time_reserved: integer, amount of time reserved in minutes
        :return: Reservation/None, whether or not it was reserved
        """
        if self.current_reservation():
            return None
        else:
            time_reserved = int(Decimal(amount_paid) / self.price_per_hour
                                * 60)
            return Reservation.objects.create(
                reserved_at=timezone.now(),
                time_reserved=time_reserved,
                parking=self,
                user=user)

    @classmethod
    def all_within_radius(cls, latitude, longitude, radius=1):
        """
        Return all parking spots within a radius of a given address.
        :param address: string
        :param radius: integer, in miles
        """
        point = Point(latitude, longitude)
        north_lat = cls._get_cords(point, radius, 'north')[0]
        south_lat = cls._get_cords(point, radius, 'south')[0]
        west_long = cls._get_cords(point, radius, 'west')[1]
        east_long = cls._get_cords(point, radius, 'east')[1]
        return Parking.objects.filter(longitude__gte=west_long,
                                      longitude__lte=east_long,
                                      latitude__lte=north_lat,
                                      latitude__gte=south_lat)

    @classmethod
    def _get_cords(cls, point, radius, direction):
        direction_to_bearing = {'north': 0,
                                'east': 90,
                                'south': 180,
                                'west': 270}
        bearing = direction_to_bearing[direction]
        return VincentyDistance(miles=radius).destination(point, bearing)


class Reservation(models.Model):
    reserved_at = models.DateTimeField(null=True, blank=True)
    # in minutes
    time_reserved = models.IntegerField(null=True, blank=True,
                                        validators=[MinValueValidator(0)])
    parking = models.ForeignKey(Parking, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def is_active(self):
        """
        Check if this reservation is active.
        """
        now = timezone.now()
        after_reserved_time = now > self.reserved_at
        before_expired_time = now <= self.expired_time
        return after_reserved_time and before_expired_time

    def extend(self, minutes):
        self.time_reserved += minutes
        self.save()

    @property
    def expired_time(self):
        return self.reserved_at + timedelta(minutes=self.time_reserved)

    def cancel(self):
        self.time_reserved = 0
        self.save()


# Extend reservation. Want to extend it by a certain amoung of time
# Charge in stripe or braintree, use dummy credit cards, add field.

# Demo, view all spots, create reservation, end reservation, charge reservation.
