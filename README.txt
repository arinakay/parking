Install the requirements
pip install -r requirements.txt

Load initial data (parking spots around the White House)
./manage.py loaddata parking.json

Run server
Server now runs on http://localhost:8000/
./manage.py runserver

Try out these endpoints:
Create a user:
POST to http://localhost:8000/user/
- Create user data, you need an email address, phone number, password
ex: {"email": "foobar@example.com", "phone": "5555555555", "password": "password123"}

Parking spot viewing & search interface
GET from http://localhost:8000/parking/<?address=some_address>
GET from http://localhost:8000/parking/<pk>/

Parking spot reservation
Start reservation
POST to http://localhost:8000/parking/<pk>/reserve/ the amount paid
ex: {"paid": 10.00}

Cancel reservation
GET from http://localhost:8000/reservation/<pk>/cancel/
